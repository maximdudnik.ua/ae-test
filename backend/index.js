const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const db = require('nedb-promise')();
const validate = require('./validation');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use( (req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8080');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});

let account = 0;

app.get('/transactions', async (req, res) => {
  try {
    const data = await db.find({});

    res.json({
      items: data,
      account
    });
  } catch (e) {
    console.error(e);
    res.status(500).end()
  }
});

app.post('/transactions', async (req, res) => {
  let { type, amount } = req.body;

  try {
    await validate(req.body);

    const increment = type === 'debit' ? amount : -amount;
    if ((account + increment) < 0) {
      return res.status(400).end()
    }

    const data = await db.insert(req.body);
    account += increment;

    res.status(201).json(data);
  } catch (e) {
    console.log(e);
    res.status(500).end();
  }
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});
