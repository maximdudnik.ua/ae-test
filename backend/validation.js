const Ajv = require('ajv');
const ajv = new Ajv(); // options can be passed, e.g. {allErrors: true}
const validate = ajv.compile({
    "$async": true,
    properties: {
        'amount': { type: 'integer' },
        'type': { type: 'string' },
        'merchant': { type: 'string' },
        'account_number': { type: 'integer' }
    },
    required: ['amount', 'type', 'merchant', 'account_number']
});

module.exports = validate;
