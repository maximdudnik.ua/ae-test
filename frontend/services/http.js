import http from 'axios';

http.defaults.withCredentials = true;
http.defaults.port = 3000;

export default http
